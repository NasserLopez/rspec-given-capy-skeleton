Installation:

0) You might need to do these steps first.
	0) install x-code:
		In a terminal run the following command: xcode-select --install  
	1) install git:
		http://git-scm.com/
	2) make sure you have phantoms installed (optional if you want to run tests headless) 
		mac:
			Suggested Method:
				- download from phantoms from: https://github.com/eugene1g/phantomjs/releases ( only build that currently works in Yosemite)
				- Place the downloaded executable found in the bin folder in your /usr/bin folder

			Another Method: 
				- install homebrew: http://brew.sh/
				- cmd: brew install phantomjs
		Other:
			- http://phantomjs.org/

1) download repo from github: 
	0) from command line: git clone https://NasserLopez@bitbucket.org/NasserLopez/rspec-given-capy-skeleton.git


2) Install Gems:
	0) From command line, change directory to the cloned folder.
	1) Install nokogiri from cmd line: gem install nokogiri
	2) install Gems from gem file from command line:  bundle install

Run Tests:
	Test Rspec installation:
		0) From inside the code folder run command: spec
	
	Other ways to run tests:
		using the Parallel-Test gem ( https://github.com/grosser/parallel_tests )
			cmd: parallel_rspec spec/
			cmd using tags: parallel_rspec spec/ --test-options '--tag focus'

	Running tests using Sublime and the ‘Ruby Tests’ plugin ( https://github.com/maltize/sublime-text-2-ruby-tests )
		1) download sublime text edit 3 from http://www.sublimetext.com/3
		2) install package control:
			0) Open Sublime 
			1) go to view > show console
			3) copy/paste the following code to the terminal and press ‘enter’.
				  import urllib.request,os,hashlib; h = 'eb2297e1a458f27d836c04bb0cbaf282' + 'd0e7a3098092775ccb37ca9d6b2e4b7d'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
			4) Restart Sublime
		3) Install ‘Ruby Tests’ Sublime plugin
			0) Open Sublime
			1) go to Tools > Common Palette
			2) search for the “Package Control: Install Package” command
			3) search for “Sublime Text 2 Ruby Tests”
			4) Select and press “Enter” to install

		Plugin Usage: ( https://github.com/maltize/sublime-text-2-ruby-tests )
			Run single ruby test: Command-Shift-R
			Run all ruby tests from current file: Command-Shift-T
			Run last ruby test(s): Command-Shift-E
			Show test panel: Command-Shift-X (when test panel visible hit esc to hide it)


		Solutions to encountered issues when trying to run tests:
			- the Cliver gem is unable to find Phantomjs in $PATH. 
				solution: add the phantomjs executable int the /usr/bin forlder or add executable to the $PATH.

	(Optional) Setting up Sublime's 'Ruby Test' plugin to run with "parallel-test" gem:
		0) Open Sulime
		1) Go to Sublime Text > Preferences > Package Settings > Ruby Test
		2) copy the contect from 'settings - default' to 'settings - user'
		3) replace line: 
				"run_rspec_command": "rspec {relative_path}",
					width
				"run_rspec_command": "parallel_rspec {relative_path}",


