require 'rspec/given'
require 'capybara'
require 'selenium/webdriver'
require 'capybara/poltergeist'
require 'yaml'

# Load config file
config = YAML.load_file('config.yml')

RSpec.configure do |config|
  config.include Capybara::DSL
end


if (config["run-headless"]) 

	# Run headless

	Capybara.register_driver :poltergeist do |app|
	Capybara::Poltergeist::Driver.new(
	  app,
	  window_size: [1280, 1024],
	  debug:       true
	)
	end
	# Capybara.default_driver    = :poltergeist
	Capybara.javascript_driver = :poltergeist


	RSpec.configure do |config|

	config.before(:all) do
	  @Capybara = Capybara::Session.new(:poltergeist)
	  Capybara.default_driver = :poltergeist
	end
	end
else 

	# run using FireFox 
	
	RSpec.configure do |config|

	config.before(:all) do
	  @Capybara = Capybara::Session.new(:selenium)
	  Capybara.default_driver = :selenium
	end
	end

end

