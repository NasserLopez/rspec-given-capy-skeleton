require 'spec_helper'
require 'yaml'

class PageObjectExample

URL = YAML.load_file('config.yml')['base-url']

def initialize(session)
@session = session
end

def visit
@session.visit URL
end

end